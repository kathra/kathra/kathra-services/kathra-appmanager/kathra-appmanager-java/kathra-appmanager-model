
# Commit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id |  [optional]
**author** | **String** | author |  [optional]
**message** | **String** | message |  [optional]
**hash** | **String** | hash |  [optional]
**date** | **String** | date |  [optional]



