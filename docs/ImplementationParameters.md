
# ImplementationParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Implementation&#39;s name | 
**apiVersion** | [**ApiVersion**](ApiVersion.md) |  | 
**language** | **String** | The language to use for source code generation | 
**desc** | **String** | Implementation&#39;s description |  [optional]



