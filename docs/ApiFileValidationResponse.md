
# ApiFileValidationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | [**FormatEnum**](#FormatEnum) | format |  [optional]
**name** | **String** | Service&#39;s name |  [optional]
**groupId** | **String** | Service&#39;s groupId |  [optional]
**version** | **String** | Service&#39;s version |  [optional]
**operations** | **Boolean** | Contains operations |  [optional]
**model** | **Boolean** | Contains model |  [optional]


<a name="FormatEnum"></a>
## Enum: FormatEnum
Name | Value
---- | -----
JSON | &quot;json&quot;
YAML | &quot;yaml&quot;
INVALID | &quot;invalid&quot;



