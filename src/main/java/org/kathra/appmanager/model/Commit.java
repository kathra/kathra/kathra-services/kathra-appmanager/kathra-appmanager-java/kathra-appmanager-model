/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* Commit
*/
@ApiModel(description = "Commit")
public class Commit {

    protected String id = null;
    protected String author = null;
    protected String message = null;
    protected String hash = null;
    protected String date = null;


    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myCommit.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Commit The modified Commit object
    **/
    public Commit id(String id) {
        this.id = id;
        return this;
    }

    /**
    * id
    * @return String id
    **/
    @ApiModelProperty(value = "id")
    public String getId() {
        return id;
    }

    /**
    * id
    **/
    public void setId(String id) {
        this.id = id;
    }

    /**
    * Quick Set author
    * Useful setter to use in builder style (eg. myCommit.author(String).anotherQuickSetter(..))
    * @param String author
    * @return Commit The modified Commit object
    **/
    public Commit author(String author) {
        this.author = author;
        return this;
    }

    /**
    * author
    * @return String author
    **/
    @ApiModelProperty(value = "author")
    public String getAuthor() {
        return author;
    }

    /**
    * author
    **/
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
    * Quick Set message
    * Useful setter to use in builder style (eg. myCommit.message(String).anotherQuickSetter(..))
    * @param String message
    * @return Commit The modified Commit object
    **/
    public Commit message(String message) {
        this.message = message;
        return this;
    }

    /**
    * message
    * @return String message
    **/
    @ApiModelProperty(value = "message")
    public String getMessage() {
        return message;
    }

    /**
    * message
    **/
    public void setMessage(String message) {
        this.message = message;
    }

    /**
    * Quick Set hash
    * Useful setter to use in builder style (eg. myCommit.hash(String).anotherQuickSetter(..))
    * @param String hash
    * @return Commit The modified Commit object
    **/
    public Commit hash(String hash) {
        this.hash = hash;
        return this;
    }

    /**
    * hash
    * @return String hash
    **/
    @ApiModelProperty(value = "hash")
    public String getHash() {
        return hash;
    }

    /**
    * hash
    **/
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
    * Quick Set date
    * Useful setter to use in builder style (eg. myCommit.date(String).anotherQuickSetter(..))
    * @param String date
    * @return Commit The modified Commit object
    **/
    public Commit date(String date) {
        this.date = date;
        return this;
    }

    /**
    * date
    * @return String date
    **/
    @ApiModelProperty(value = "date")
    public String getDate() {
        return date;
    }

    /**
    * date
    **/
    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Commit commit = (Commit) o;
        return Objects.equals(this.id, commit.id) &&
        Objects.equals(this.author, commit.author) &&
        Objects.equals(this.message, commit.message) &&
        Objects.equals(this.hash, commit.hash) &&
        Objects.equals(this.date, commit.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author, message, hash, date);
    }
}

