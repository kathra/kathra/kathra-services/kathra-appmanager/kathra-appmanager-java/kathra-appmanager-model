/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* ApiFileValidationResponse
*/
@ApiModel(description = "ApiFileValidationResponse")
public class ApiFileValidationResponse {

    /**
    * format
    */
    public enum FormatEnum {
        JSON("json"),
        
        YAML("yaml"),
        
        INVALID("invalid");

        private String value;

        FormatEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static FormatEnum fromValue(String text) {
            for (FormatEnum b : FormatEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected FormatEnum format = null;
    protected String name = null;
    protected String groupId = null;
    protected String version = null;
    protected Boolean operations = null;
    protected Boolean model = null;


    /**
    * Quick Set format
    * Useful setter to use in builder style (eg. myApiFileValidationResponse.format(FormatEnum).anotherQuickSetter(..))
    * @param FormatEnum format
    * @return ApiFileValidationResponse The modified ApiFileValidationResponse object
    **/
    public ApiFileValidationResponse format(FormatEnum format) {
        this.format = format;
        return this;
    }

    /**
    * format
    * @return FormatEnum format
    **/
    @ApiModelProperty(value = "format")
    public FormatEnum getFormat() {
        return format;
    }

    /**
    * format
    **/
    public void setFormat(FormatEnum format) {
        this.format = format;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myApiFileValidationResponse.name(String).anotherQuickSetter(..))
    * @param String name
    * @return ApiFileValidationResponse The modified ApiFileValidationResponse object
    **/
    public ApiFileValidationResponse name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Service's name
    * @return String name
    **/
    @ApiModelProperty(value = "Service's name")
    public String getName() {
        return name;
    }

    /**
    * Service's name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set groupId
    * Useful setter to use in builder style (eg. myApiFileValidationResponse.groupId(String).anotherQuickSetter(..))
    * @param String groupId
    * @return ApiFileValidationResponse The modified ApiFileValidationResponse object
    **/
    public ApiFileValidationResponse groupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    /**
    * Service's groupId
    * @return String groupId
    **/
    @ApiModelProperty(value = "Service's groupId")
    public String getGroupId() {
        return groupId;
    }

    /**
    * Service's groupId
    **/
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
    * Quick Set version
    * Useful setter to use in builder style (eg. myApiFileValidationResponse.version(String).anotherQuickSetter(..))
    * @param String version
    * @return ApiFileValidationResponse The modified ApiFileValidationResponse object
    **/
    public ApiFileValidationResponse version(String version) {
        this.version = version;
        return this;
    }

    /**
    * Service's version
    * @return String version
    **/
    @ApiModelProperty(value = "Service's version")
    public String getVersion() {
        return version;
    }

    /**
    * Service's version
    **/
    public void setVersion(String version) {
        this.version = version;
    }

    /**
    * Quick Set operations
    * Useful setter to use in builder style (eg. myApiFileValidationResponse.operations(Boolean).anotherQuickSetter(..))
    * @param Boolean operations
    * @return ApiFileValidationResponse The modified ApiFileValidationResponse object
    **/
    public ApiFileValidationResponse operations(Boolean operations) {
        this.operations = operations;
        return this;
    }

    /**
    * Contains operations
    * @return Boolean operations
    **/
    @ApiModelProperty(value = "Contains operations")
    public Boolean isOperations() {
        return operations;
    }

    /**
    * Contains operations
    **/
    public void setOperations(Boolean operations) {
        this.operations = operations;
    }

    /**
    * Quick Set model
    * Useful setter to use in builder style (eg. myApiFileValidationResponse.model(Boolean).anotherQuickSetter(..))
    * @param Boolean model
    * @return ApiFileValidationResponse The modified ApiFileValidationResponse object
    **/
    public ApiFileValidationResponse model(Boolean model) {
        this.model = model;
        return this;
    }

    /**
    * Contains model
    * @return Boolean model
    **/
    @ApiModelProperty(value = "Contains model")
    public Boolean isModel() {
        return model;
    }

    /**
    * Contains model
    **/
    public void setModel(Boolean model) {
        this.model = model;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ApiFileValidationResponse apiFileValidationResponse = (ApiFileValidationResponse) o;
        return Objects.equals(this.format, apiFileValidationResponse.format) &&
        Objects.equals(this.name, apiFileValidationResponse.name) &&
        Objects.equals(this.groupId, apiFileValidationResponse.groupId) &&
        Objects.equals(this.version, apiFileValidationResponse.version) &&
        Objects.equals(this.operations, apiFileValidationResponse.operations) &&
        Objects.equals(this.model, apiFileValidationResponse.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(format, name, groupId, version, operations, model);
    }
}

