/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.kathra.core.model.ApiVersion;
import java.util.Objects;

/**
* ImplementationParameters
*/
public class ImplementationParameters {

    protected String name = null;
    protected ApiVersion apiVersion = null;
    protected String language = null;
    protected String desc = null;


    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myImplementationParameters.name(String).anotherQuickSetter(..))
    * @param String name
    * @return ImplementationParameters The modified ImplementationParameters object
    **/
    public ImplementationParameters name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Implementation's name
    * @return String name
    **/
    @ApiModelProperty(required = true, value = "Implementation's name")
    public String getName() {
        return name;
    }

    /**
    * Implementation's name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set apiVersion
    * Useful setter to use in builder style (eg. myImplementationParameters.apiVersion(ApiVersion).anotherQuickSetter(..))
    * @param ApiVersion apiVersion
    * @return ImplementationParameters The modified ImplementationParameters object
    **/
    public ImplementationParameters apiVersion(ApiVersion apiVersion) {
        this.apiVersion = apiVersion;
        return this;
    }

    /**
    * Get apiVersion
    * @return ApiVersion apiVersion
    **/
    @ApiModelProperty(required = true, value = "")
    public ApiVersion getApiVersion() {
        return apiVersion;
    }

    /**
    * Set apiVersion
    * @param ApiVersion apiVersion
    **/
    public void setApiVersion(ApiVersion apiVersion) {
        this.apiVersion = apiVersion;
    }

    /**
    * Quick Set language
    * Useful setter to use in builder style (eg. myImplementationParameters.language(String).anotherQuickSetter(..))
    * @param String language
    * @return ImplementationParameters The modified ImplementationParameters object
    **/
    public ImplementationParameters language(String language) {
        this.language = language;
        return this;
    }

    /**
    * The language to use for source code generation
    * @return String language
    **/
    @ApiModelProperty(required = true, value = "The language to use for source code generation")
    public String getLanguage() {
        return language;
    }

    /**
    * The language to use for source code generation
    **/
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
    * Quick Set desc
    * Useful setter to use in builder style (eg. myImplementationParameters.desc(String).anotherQuickSetter(..))
    * @param String desc
    * @return ImplementationParameters The modified ImplementationParameters object
    **/
    public ImplementationParameters desc(String desc) {
        this.desc = desc;
        return this;
    }

    /**
    * Implementation's description
    * @return String desc
    **/
    @ApiModelProperty(value = "Implementation's description")
    public String getDesc() {
        return desc;
    }

    /**
    * Implementation's description
    **/
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ImplementationParameters implementationParameters = (ImplementationParameters) o;
        return Objects.equals(this.name, implementationParameters.name) &&
        Objects.equals(this.apiVersion, implementationParameters.apiVersion) &&
        Objects.equals(this.language, implementationParameters.language) &&
        Objects.equals(this.desc, implementationParameters.desc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, apiVersion, language, desc);
    }
}

