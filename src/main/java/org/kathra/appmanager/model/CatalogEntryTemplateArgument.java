/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* CatalogEntryTemplateArgument
*/
@ApiModel(description = "CatalogEntryTemplateArgument")
public class CatalogEntryTemplateArgument {

    protected String key = null;
    protected String label = null;
    protected String value = null;
    protected String contrainst = null;


    /**
    * Quick Set key
    * Useful setter to use in builder style (eg. myCatalogEntryTemplateArgument.key(String).anotherQuickSetter(..))
    * @param String key
    * @return CatalogEntryTemplateArgument The modified CatalogEntryTemplateArgument object
    **/
    public CatalogEntryTemplateArgument key(String key) {
        this.key = key;
        return this;
    }

    /**
    * Argument key to generate codegen
    * @return String key
    **/
    @ApiModelProperty(value = "Argument key to generate codegen")
    public String getKey() {
        return key;
    }

    /**
    * Argument key to generate codegen
    **/
    public void setKey(String key) {
        this.key = key;
    }

    /**
    * Quick Set label
    * Useful setter to use in builder style (eg. myCatalogEntryTemplateArgument.label(String).anotherQuickSetter(..))
    * @param String label
    * @return CatalogEntryTemplateArgument The modified CatalogEntryTemplateArgument object
    **/
    public CatalogEntryTemplateArgument label(String label) {
        this.label = label;
        return this;
    }

    /**
    * Argument label
    * @return String label
    **/
    @ApiModelProperty(value = "Argument label")
    public String getLabel() {
        return label;
    }

    /**
    * Argument label
    **/
    public void setLabel(String label) {
        this.label = label;
    }

    /**
    * Quick Set value
    * Useful setter to use in builder style (eg. myCatalogEntryTemplateArgument.value(String).anotherQuickSetter(..))
    * @param String value
    * @return CatalogEntryTemplateArgument The modified CatalogEntryTemplateArgument object
    **/
    public CatalogEntryTemplateArgument value(String value) {
        this.value = value;
        return this;
    }

    /**
    * Argument value to generate codegen
    * @return String value
    **/
    @ApiModelProperty(value = "Argument value to generate codegen")
    public String getValue() {
        return value;
    }

    /**
    * Argument value to generate codegen
    **/
    public void setValue(String value) {
        this.value = value;
    }

    /**
    * Quick Set contrainst
    * Useful setter to use in builder style (eg. myCatalogEntryTemplateArgument.contrainst(String).anotherQuickSetter(..))
    * @param String contrainst
    * @return CatalogEntryTemplateArgument The modified CatalogEntryTemplateArgument object
    **/
    public CatalogEntryTemplateArgument contrainst(String contrainst) {
        this.contrainst = contrainst;
        return this;
    }

    /**
    * Argument constraint
    * @return String contrainst
    **/
    @ApiModelProperty(value = "Argument constraint")
    public String getContrainst() {
        return contrainst;
    }

    /**
    * Argument constraint
    **/
    public void setContrainst(String contrainst) {
        this.contrainst = contrainst;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatalogEntryTemplateArgument catalogEntryTemplateArgument = (CatalogEntryTemplateArgument) o;
        return Objects.equals(this.key, catalogEntryTemplateArgument.key) &&
        Objects.equals(this.label, catalogEntryTemplateArgument.label) &&
        Objects.equals(this.value, catalogEntryTemplateArgument.value) &&
        Objects.equals(this.contrainst, catalogEntryTemplateArgument.contrainst);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, label, value, contrainst);
    }
}

