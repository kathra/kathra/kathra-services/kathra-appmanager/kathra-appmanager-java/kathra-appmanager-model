/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.appmanager.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* CatalogEntryTemplate
*/
@ApiModel(description = "CatalogEntryTemplate")
public class CatalogEntryTemplate {

    protected String name = null;
    protected String label = null;
    protected List<CatalogEntryTemplateArgument> arguments = null;


    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myCatalogEntryTemplate.name(String).anotherQuickSetter(..))
    * @param String name
    * @return CatalogEntryTemplate The modified CatalogEntryTemplate object
    **/
    public CatalogEntryTemplate name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Template Name
    * @return String name
    **/
    @ApiModelProperty(value = "Template Name")
    public String getName() {
        return name;
    }

    /**
    * Template Name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set label
    * Useful setter to use in builder style (eg. myCatalogEntryTemplate.label(String).anotherQuickSetter(..))
    * @param String label
    * @return CatalogEntryTemplate The modified CatalogEntryTemplate object
    **/
    public CatalogEntryTemplate label(String label) {
        this.label = label;
        return this;
    }

    /**
    * Template Label
    * @return String label
    **/
    @ApiModelProperty(value = "Template Label")
    public String getLabel() {
        return label;
    }

    /**
    * Template Label
    **/
    public void setLabel(String label) {
        this.label = label;
    }

    /**
    * Quick Set arguments
    * Useful setter to use in builder style (eg. myCatalogEntryTemplate.arguments(List<CatalogEntryTemplateArgument>).anotherQuickSetter(..))
    * @param List<CatalogEntryTemplateArgument> arguments
    * @return CatalogEntryTemplate The modified CatalogEntryTemplate object
    **/
    public CatalogEntryTemplate arguments(List<CatalogEntryTemplateArgument> arguments) {
        this.arguments = arguments;
        return this;
    }

    public CatalogEntryTemplate addArgumentsItem(CatalogEntryTemplateArgument argumentsItem) {
        if (this.arguments == null) {
            this.arguments = new ArrayList();
        }
        this.arguments.add(argumentsItem);
        return this;
    }

    /**
    * CodeGen Argument
    * @return List<CatalogEntryTemplateArgument> arguments
    **/
    @ApiModelProperty(value = "CodeGen Argument")
    public List<CatalogEntryTemplateArgument> getArguments() {
        return arguments;
    }

    /**
    * CodeGen Argument
    **/
    public void setArguments(List<CatalogEntryTemplateArgument> arguments) {
        this.arguments = arguments;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatalogEntryTemplate catalogEntryTemplate = (CatalogEntryTemplate) o;
        return Objects.equals(this.name, catalogEntryTemplate.name) &&
        Objects.equals(this.label, catalogEntryTemplate.label) &&
        Objects.equals(this.arguments, catalogEntryTemplate.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, label, arguments);
    }
}

